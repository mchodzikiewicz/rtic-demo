use embedded_plots::curve::PlotPoint;

pub struct Shape {
    pub plot: &'static [PlotPoint],
}

static SHAPE1_PLOT: [PlotPoint; 6] = [
    PlotPoint { x: 0, y: 0 },
    PlotPoint { x: 1, y: 0 },
    PlotPoint { x: 1, y: 2 },
    PlotPoint { x: 2, y: 2 },
    PlotPoint { x: 2, y: 0 },
    PlotPoint { x: 3, y: 0 },
];

static SHAPE2_PLOT: [PlotPoint; 8] = [
    PlotPoint { x: 0, y: 0 },
    PlotPoint { x: 1, y: 0 },
    PlotPoint { x: 1, y: 1 },
    PlotPoint { x: 2, y: 1 },
    PlotPoint { x: 2, y: 2 },
    PlotPoint { x: 3, y: 2 },
    PlotPoint { x: 3, y: 0 },
    PlotPoint { x: 4, y: 0 },
];

pub static SHAPES: [Shape;2] = [
    Shape {
        plot: &SHAPE1_PLOT,
    },
    Shape {
        plot: &SHAPE2_PLOT,
    },
];