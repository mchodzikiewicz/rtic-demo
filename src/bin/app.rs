#![no_main]
#![no_std]
#![feature(exclusive_range_pattern)]

use rtic_demo as _; // global logger + panicking-behavior + memory layout

#[rtic::app(device = stm32f7xx_hal::pac, dispatchers = [CAN1_TX, CAN1_RX0])]
mod app {
    use systick_monotonic::*;

    #[monotonic(binds = SysTick, default = true)]
    type MyMono = Systick<1000>;

    use embedded_hal::serial::{Read, Write};
    use stm32f7xx_hal::pac::USART1;
    use stm32f7xx_hal::serial::{Rx, Tx};


    use stm32f7xx_hal::gpio::{Output, PushPull, PA0};

    use embedded_plots::curve::PlotPoint;

    use rtic_demo::shapes::SHAPES;
    use rtic_demo::{peripherals, redraw};
    use rtic_demo::screen::{Stm32F7DiscoDisplay};

    #[shared]
    struct Shared {
        pfm_period: fugit::MillisDurationU64,
    }

    #[local]
    struct Local
    {
        display: Stm32F7DiscoDisplay<u16>,
        tx: Tx<USART1>,
        rx: Rx<USART1>,
        pfm_pin: PA0<Output<PushPull>>,
        pfm_state: bool,
    }

    #[init]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        defmt::info!("init");
        let systick = cx.core.SYST;
        let mut delay = cortex_m::delay::Delay::new(systick, 216_000_000);
        let (display, tx, rx, pfm_pin)
            = peripherals::init(cx.device, &mut delay);
        let systick = delay.free();
        let mono = Systick::new(systick, 216_000_000);
        lcd_redraw::spawn(SHAPES[0].plot).ok();
        pfm_handler::spawn().ok();

        (
            Shared {
                pfm_period: 9.millis(),
            },
            Local {
                display,
                rx,
                tx,
                pfm_pin,
                pfm_state: false,
            },
            init::Monotonics(
                mono,
            ),
        )
    }

    #[idle]
    fn idle(_: idle::Context) -> ! {
        defmt::info!("idle");
        loop {
            continue;
        }
    }

    #[task(priority = 3, local = [display])]
    fn lcd_redraw(cx: lcd_redraw::Context, shape: &'static [PlotPoint]) {
        let display = cx.local.display;
        redraw::redraw(shape, display);
    }

    #[task(priority = 1, local = [pfm_pin, pfm_state], shared = [pfm_period])]
    fn pfm_handler(mut cx: pfm_handler::Context) {
        let pin = cx.local.pfm_pin;
        let state = cx.local.pfm_state;
        let period = cx.shared.pfm_period.lock(|p| *p);

        match state {
            true => {
                pin.set_low();
                *state = false;
            },
            false => {
                pin.set_high();
                *state = true;
            },
        }
        ;
        pfm_handler::spawn_after(period).ok();
    }

    #[task(priority = 2, binds = USART1, local = [rx, tx], shared = [pfm_period])]
    fn usart1(mut cx: usart1::Context) {
        match cx.local.rx.read() {
            Ok(byte) => {
                match byte as char {
                    'a' => {
                        lcd_redraw::spawn(SHAPES[0].plot).ok();
                        defmt::info!("Changed shape to A");
                    }
                    'b' => {
                        lcd_redraw::spawn(&SHAPES[1].plot).ok();
                        defmt::info!("Changed shape to B");
                    }
                    '1'..='9' => {
                        let period = (byte as char).to_digit(10).unwrap();
                        cx.shared.pfm_period.lock(|p| *p = (period as u64).millis());
                        defmt::info!("Changed period to {}",period);
                    }
                    _ => (),
                }
                cx.local.tx.write(byte).ok();
            }
            _ => (),
        }
    }
}