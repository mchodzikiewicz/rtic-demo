use core::{mem, slice};
use cortex_m::delay::Delay;

use systick_monotonic::fugit::RateExtU32;

use stm32f7xx_hal::pac::{Peripherals, USART1};
use stm32f7xx_hal::rcc::{HSEClock, HSEClockMode, Rcc, RccExt};
use stm32f7xx_hal::fmc::FmcExt;
use stm32f7xx_hal::gpio::{GpioExt, Output, PA0, PushPull, Speed};
use stm32f7xx_hal::ltdc::{Layer, PixelFormat};
use stm32f7xx_hal::serial::{Event, Rx, Tx};

use stm32_fmc::devices::is42s16400j_7;

use crate::screen::Stm32F7DiscoDisplay;

// LCD screen resolution
const WIDTH: u16 = 480;
const HEIGHT: u16 = 272;

// framebuffer consts
const FB_GRAPHICS_SIZE: usize = (WIDTH as usize) * (HEIGHT as usize);

// sdram config macro
macro_rules! fmc_pins {
    ($($pin:expr),*) => {
        (
            $(
                $pin.into_push_pull_output()
                    .set_speed(Speed::VeryHigh)
                    .into_alternate()
                    .internal_pull_up(true)
            ),*
        )
    };
}

pub fn init(peripherals: Peripherals, delay: &mut Delay) -> (Stm32F7DiscoDisplay<u16>, Tx<USART1>, Rx<USART1>, PA0<Output<PushPull>>) {
    // setup clocks
    let rcc_hal: Rcc = peripherals.RCC.constrain();
    let clocks = rcc_hal
        .cfgr
        .hse(HSEClock::new(25_000_000.Hz(), HSEClockMode::Bypass))
        .sysclk(216_000_000.Hz())
        .hclk(216_000_000.Hz())
        .freeze();

    // get all io pins
    let gpioa = peripherals.GPIOA.split();
    let gpiob = peripherals.GPIOB.split();
    let gpioc = peripherals.GPIOC.split();
    let gpiod = peripherals.GPIOD.split();
    let gpioe = peripherals.GPIOE.split();
    let gpiof = peripherals.GPIOF.split();
    let gpiog = peripherals.GPIOG.split();
    let gpioh = peripherals.GPIOH.split();
    let gpioi = peripherals.GPIOI.split();
    let gpioj = peripherals.GPIOJ.split();
    let gpiok = peripherals.GPIOK.split();

    let tx = gpioa.pa9.into_alternate();
    let rx = gpiob.pb7.into_alternate();

    // setup serial port (USART)
    let mut serial = stm32f7xx_hal::serial::Serial::new(
        peripherals.USART1,
        (tx, rx),
        &clocks,
        stm32f7xx_hal::serial::Config {
            // Default to 115_200 bauds
            ..Default::default()
        },
    );
    serial.listen(Event::Rxne);
    let (tx,rx) = serial.split();

    //setup output pin for generating square signal
    let pfm_pin = gpioa.pa0.into_push_pull_output();

    // initialize sdram
    let fmc_io = fmc_pins! {
        gpiof.pf0,  // A0
        gpiof.pf1,  // A1
        gpiof.pf2,  // A2
        gpiof.pf3,  // A3
        gpiof.pf4,  // A4
        gpiof.pf5,  // A5
        gpiof.pf12, // A6
        gpiof.pf13, // A7
        gpiof.pf14, // A8
        gpiof.pf15, // A9
        gpiog.pg0,  // A10
        gpiog.pg1,  // A11
        gpiog.pg4,  // BA0
        gpiog.pg5,  // BA1
        gpiod.pd14, // D0
        gpiod.pd15, // D1
        gpiod.pd0,  // D2
        gpiod.pd1,  // D3
        gpioe.pe7,  // D4
        gpioe.pe8,  // D5
        gpioe.pe9,  // D6
        gpioe.pe10, // D7
        gpioe.pe11, // D8
        gpioe.pe12, // D9
        gpioe.pe13, // D10
        gpioe.pe14, // D11
        gpioe.pe15, // D12
        gpiod.pd8,  // D13
        gpiod.pd9,  // D14
        gpiod.pd10, // D15
        gpioe.pe0,  // NBL0
        gpioe.pe1,  // NBL1
        gpioc.pc3,  // SDCKEn
        gpiog.pg8,  // SDCLK
        gpiog.pg15, // SDNCAS
        gpioh.ph3,  // SDNEn
        gpiof.pf11, // SDNRAS
        gpioh.ph5   // SDNWE
    };
    let mut sdram = peripherals.FMC.sdram(fmc_io, is42s16400j_7::Is42s16400j {}, &clocks);


    //initialize LCD
    gpioe.pe4.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_B0
    gpiog.pg12.into_alternate::<9>().set_speed(Speed::VeryHigh); // LTCD_B4
    gpioi.pi9.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_VSYNC
    gpioi.pi10.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_HSYNC
    gpioi.pi13.into_alternate::<14>().set_speed(Speed::VeryHigh);
    gpioi.pi14.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_CLK
    gpioi.pi15.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_R0
    gpioj.pj0.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_R1
    gpioj.pj1.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_R2
    gpioj.pj2.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_R3
    gpioj.pj3.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_R4
    gpioj.pj4.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_R5
    gpioj.pj5.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_R6
    gpioj.pj6.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_R7
    gpioj.pj7.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_G0
    gpioj.pj8.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_G1
    gpioj.pj9.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_G2
    gpioj.pj10.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_G3
    gpioj.pj11.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_G4
    gpioj.pj13.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_B1
    gpioj.pj14.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_B2
    gpioj.pj15.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_B3

    gpiok.pk0.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_G5
    gpiok.pk1.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_G6
    gpiok.pk2.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_G7
    gpiok.pk4.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_B5
    gpiok.pk5.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_B6
    gpiok.pk6.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_D7
    gpiok.pk7.into_alternate::<14>().set_speed(Speed::VeryHigh); // LTCD_E
    gpioh.ph1.into_floating_input();

    // lcd control pins
    let mut disp_on = gpioi.pi12.into_push_pull_output();
    disp_on.set_low();
    let mut backlight = gpiok.pk3.into_push_pull_output();
    backlight.set_high();

    // setup framebuffer
    let len_bytes = (16 * 1024 * 1024) / 2;
    let len_words = len_bytes / mem::size_of::<u32>();
    let ram = unsafe {
        let ram_ptr: *mut u32 = sdram.init(delay);
        slice::from_raw_parts_mut(ram_ptr, len_words)
    };
    let fbuffer1 : &mut [u16] = unsafe { mem::transmute(&mut ram[0..FB_GRAPHICS_SIZE]) };

    //setup display controller
    let mut display = Stm32F7DiscoDisplay::new(peripherals.LTDC, peripherals.DMA2D);
    display
        .controller
        .config_layer(Layer::L1, fbuffer1, PixelFormat::RGB565);

    display.controller.enable_layer(Layer::L1);
    display.controller.reload();

    disp_on.set_high();

    (display, tx, rx, pfm_pin)
}