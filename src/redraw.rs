use core::fmt::Debug;
use cortex_m::asm::delay;

use embedded_graphics::{
    DrawTarget,
    pixelcolor::RgbColor,
};

use embedded_graphics::drawable::Drawable;
use embedded_graphics::geometry::Point;

use embedded_plots::axis::Scale;
use embedded_plots::curve::{Curve, PlotPoint};
use embedded_plots::single_plot::SinglePlot;

pub fn redraw<D, C>(plot: &[PlotPoint], display: &mut D)
where D: DrawTarget<C>,
      C: RgbColor + Default, <D as DrawTarget<C>>::Error: Debug,
{
    display.clear(RgbColor::BLACK).unwrap();
    for _ in 0..20 {
        delay(65000);
    }
    let curve = Curve::from_data(plot);
    let plot = SinglePlot::new(
        &curve,
        Scale::RangeFraction(3),
        Scale::RangeFraction(2))
        .into_drawable(
        Point { x: 25, y: 5 },
        Point { x: 450, y: 255 })
        .set_color(RgbColor::BLUE)
        .set_text_color(RgbColor::WHITE)
        .set_axis_color(RgbColor::YELLOW);
    plot.draw(display).unwrap();
}