# RTIC example for 23.06.2022 Rust Wrocław Meetup.

![image](demo.png)

The talk is available [here](https://www.youtube.com/watch?v=mzFTjn9eftU)

This example runs on [`32F746GDISCOVERY`](https://www.st.com/en/evaluation-tools/32f746gdiscovery.html) 
(some time ago, ST called it `STM32F7DISCO`, so search for both names if you need some docs and examples)

To run the example, find one of Mini USB from your obsolete camera that for some reason you still keep in one of your drawers and
plug the board.

On most Linux distros you should be fine by following these steps:
* make sure you have [correct access to the STLink device](https://calinradoni.github.io/pages/200616-non-root-access-usb.html),
* install Rust toolchain for ARM Cortex-M7F:
```sh
rustup target add thumbv7m-none-eabi
```

* execute
```sh
cargo run
```

* connect with the virtual serial port
 
To communicate with the board over the serial port, use serial port terminal of your preference, for example:
```sh
picocom -b 115200 /dev/ttyACM0
```

for other platforms, please follow Embedded Rust Book mentioned below

### Trap for young players:
If you want to see log entries provided by `defmt::info!("your log")` you need to adjust log level filter by exporting
a `DEFMT_LOG` variable in your shell.
```sh
export DEFMT_LOG=info
```

### Projects from which I took parts of code to make it work hurrying before presentation:
* [`rtic app template`](https://github.com/rtic-rs/defmt-app-template) for general shape of this app
* [`stm32f7xx_hal`](https://crates.io/crates/stm32f7xx-hal) GPIO, Serial and screen examples
* [`rtic book`](https://rtic.rs/1/book/en/) for some task attributes and so on

###  Valuable resources for anybody interested in my presentation:
* [Rust Book](https://doc.rust-lang.org/book/) if you don't know why the fuss about Rust
* [Discovery Book](https://docs.rust-embedded.org/discovery/) if you have no idea about embedded systems and want to
learn about them in Rust
* [Making Embedded Systems](https://www.oreilly.com/library/view/making-embedded-systems/9781449308889/) if you 
want to get solid foundation on how bare metal systems work
* [Embedded Rust Book](https://docs.rust-embedded.org/book/) if you want to know about idioms used in embedded systems
development in Rust
* [RTIC Book](https://rtic.rs/1/book/en/) If you want to learn how to deal with concurrent bare metal systems using RTIC
